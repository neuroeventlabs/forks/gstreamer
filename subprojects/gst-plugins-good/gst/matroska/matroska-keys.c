/* GStreamer EBML I/O
 * (c) 2019 Eckhart Koeppen <eckhart.koppen@neuroeventlabs.com>
 *
 * matroska-keys.c: Get key from key property
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include <stdlib.h>
#include <stdio.h>
#include <glib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <openssl/rand.h>
#include <openssl/pem.h>
#include "matroska-keys.h"

#define AES_KEY_SIZE 16
#define KEY_ERROR(element, msg) \
  GST_ELEMENT_ERROR (element, RESOURCE, FAILED, (msg), NULL)

static const gchar KEY_PREFIX[] = "key:";
static const gchar ID_PREFIX[] = "id:";
static const gchar ENV_PREFIX[] = "env:";
static const gchar FILE_PREFIX[] = "file:";

static const gint MAX_KEYID_SIZE = 512;
static const gint MAX_PUBKEY_SIZE = 5120;

static guint8 from_hex (gchar c);
static gboolean is_hex (gchar c);
static guint8 *prop_to_key (GstElement * element, gchar * key_prop);
static guint8 *string_to_key (GstElement * element, gchar * c);
static guint8 *file_to_key (GstElement * element, const gchar * filename);
static GstBuffer *string_to_key_id (GstElement * element, gchar * c);
static GstBuffer *file_to_key_id (GstElement * element, const gchar * filename);
static GstBuffer *prop_to_key_id (GstElement * element, gchar * key_id_prop);
static void set_key_from_prop (GstMatroskaMux * mux);
static void set_key_from_context (GstMatroskaMux * mux, GstContext * ctx);
static gboolean public_key_encrypt (GstMatroskaMux * mux, EVP_PKEY * pub_key,
    guint8 * key, guint8 * encrypted_key, gsize * encrypted_key_size);
static void generate_key (GstMatroskaMux * mux);
static void set_context (GstMatroskaMux * mux);

guint8
from_hex (gchar c)
{
  return ('0' <= c && c <= '9' ? c - '0' :
      'a' <= c && c <= 'f' ? c - 'a' + 10 :
      'A' <= c && c <= 'F' ? c - 'A' + 10 : 0);
}

gboolean
is_hex (gchar c)
{
  return (('0' <= c && c <= '9') ||
      ('a' <= c && c <= 'f') || ('A' <= c && c <= 'F'));
}

guint8 *
string_to_key (GstElement *element, gchar *c)
{
  guint8 *key = NULL;
  int i;

  if (c == NULL) {
    KEY_ERROR (element, "Invalid key property");
    goto error;
  }

  if (strlen (c) != 2 * AES_KEY_SIZE) {
    KEY_ERROR (element, "Invalid key length");
    goto error;
  }
  key = g_malloc (AES_KEY_SIZE);
  for (i = 0; i < AES_KEY_SIZE && is_hex (c[i * 2])
      && is_hex (c[i * 2 + 1]); i++) {
    key[i] = 16 * from_hex (c[i * 2]) + from_hex (c[i * 2 + 1]);
  }
  if (i < AES_KEY_SIZE) {
    KEY_ERROR (element, "Key contains invalid characters");
    g_free (key);
    goto error;
  }
  return key;

error:
  return NULL;
}

guint8 *
file_to_key (GstElement *element, const gchar *filename)
{
  FILE *f;
  struct stat st;
  guint8 *key;

  if (stat (filename, &st) == 0
      && (st.st_size < 0 || st.st_size > AES_KEY_SIZE)) {
    KEY_ERROR (element, "Key length out of range");
    goto error;
  }

  f = fopen (filename, "rb");
  if (f == NULL) {
    KEY_ERROR (element, "Failed to open key file");
    goto error;
  }

  key = g_malloc (AES_KEY_SIZE);
  if (fread (key, 1, AES_KEY_SIZE, f) != AES_KEY_SIZE) {
    g_free (key);
    fclose (f);
    KEY_ERROR (element, "Key too short");
    goto error;
  }
  fclose (f);
  return key;

error:
  return NULL;
}

guint8 *
prop_to_key (GstElement *element, gchar *key_prop)
{
  if (strncmp (key_prop, KEY_PREFIX, strlen (KEY_PREFIX)) == 0) {
    return string_to_key (element, key_prop + strlen (KEY_PREFIX));
  } else if (strncmp (key_prop, ENV_PREFIX, strlen (ENV_PREFIX)) == 0) {
    return string_to_key (element, getenv (key_prop + strlen (ENV_PREFIX)));
  } else if (strncmp (key_prop, FILE_PREFIX, strlen (FILE_PREFIX)) == 0) {
    return file_to_key (element, key_prop + strlen (FILE_PREFIX));
  }

  KEY_ERROR (element, "Invalid key property");
  return NULL;
}

GstBuffer *
string_to_key_id (GstElement *element, gchar *c)
{
  if (c != NULL) {
    return gst_buffer_new_wrapped (g_strndup (c, strlen (c)), strlen (c));
  }

  KEY_ERROR (element, "Invalid key ID property");
  return NULL;
}

GstBuffer *
file_to_key_id (GstElement *element, const gchar *filename)
{
  guint8 *key_id;
  gsize key_id_size;
  struct stat st;

  FILE *f = fopen (filename, "rb");
  if (f == NULL) {
    KEY_ERROR (element, "Failed to open key ID file");
    goto error_return;
  }

  if (stat (filename, &st) != 0 || st.st_size < 0
      || st.st_size > MAX_KEYID_SIZE) {
    KEY_ERROR (element, "Key ID size out of range");
    goto error;
  }

  key_id_size = (gsize) st.st_size;
  key_id = g_malloc (key_id_size);
  if (fread (key_id, 1, key_id_size, f) != key_id_size) {
    g_free (key_id);
    KEY_ERROR (element, "Failed to read key ID from file");
    goto error;
  }

  fclose (f);
  return gst_buffer_new_wrapped (key_id, key_id_size);

error:
  fclose (f);
error_return:
  return NULL;
}

GstBuffer *
prop_to_key_id (GstElement *element, gchar *key_id_prop)
{
  if (strncmp (key_id_prop, ID_PREFIX, strlen (ID_PREFIX)) == 0) {
    return string_to_key_id (element, key_id_prop + strlen (ID_PREFIX));
  } else if (strncmp (key_id_prop, ENV_PREFIX, strlen (ENV_PREFIX)) == 0) {
    return string_to_key_id (element, getenv
        (key_id_prop + strlen (ENV_PREFIX)));
  } else if (strncmp (key_id_prop, FILE_PREFIX, strlen (FILE_PREFIX)) == 0) {
    return file_to_key_id (element, key_id_prop + strlen (FILE_PREFIX));
  }
  KEY_ERROR (element, "Invalid key ID property");
  return NULL;
}

void
set_key_from_context (GstMatroskaMux *mux, GstContext *ctx)
{
  const GValue *v;
  const GstStructure *s = gst_context_get_structure (ctx);
  mux->key = g_value_get_boxed (gst_structure_get_value (s, "key"));
  v = gst_structure_get_value (s, "keyid");
  if (v != NULL) {
    mux->key_id = gst_value_get_buffer (v);
  }
  gst_context_unref (ctx);
}

void
set_context (GstMatroskaMux *mux)
{
  GstStructure *s;
  GValue value = G_VALUE_INIT;
  GstContext *ctx = gst_context_new ("encryption-context", FALSE);

  s = gst_context_writable_structure (ctx);
  g_value_init (&value, G_TYPE_BYTES);
  g_value_take_boxed (&value, mux->key);
  gst_structure_take_value (s, "key", &value);

  if (mux->key_id != NULL) {
    g_value_unset (&value);
    g_value_init (&value, GST_TYPE_BUFFER);
    gst_value_take_buffer (&value, mux->key_id);
    gst_structure_take_value (s, "keyid", &value);
  }

  gst_element_set_context (GST_ELEMENT_PARENT (mux), ctx);
  gst_context_unref (ctx);
}

void
set_key_from_prop (GstMatroskaMux *mux)
{
  guint8 *key = prop_to_key (&mux->element, mux->key_prop);
  if (key != NULL) {
    mux->key = g_bytes_new_take (key, AES_KEY_SIZE);
    if (mux->key_id_prop != NULL) {
      mux->key_id = prop_to_key_id (&mux->element, mux->key_id_prop);
    }
    set_context (mux);
  }
}

gboolean
public_key_encrypt (GstMatroskaMux *mux, EVP_PKEY *pub_key, guint8 *key,
    guint8 *encrypted_key, gsize *encrypted_key_size)
{
  EVP_PKEY_CTX *ctx = EVP_PKEY_CTX_new (pub_key, NULL);
  if (ctx == NULL) {
    KEY_ERROR (mux, "Failed to allocate public key\n");
    goto error_return;
  }

  if (EVP_PKEY_encrypt_init (ctx) <= 0) {
    KEY_ERROR (mux, "Failed to initialize encryption context\n");
    goto error;
  }

  if (EVP_PKEY_CTX_set_rsa_padding (ctx, RSA_PKCS1_OAEP_PADDING) <= 0) {
    KEY_ERROR (mux, "Failed to set encryption padding\n");
    goto error;
  }

  if (EVP_PKEY_encrypt (ctx, encrypted_key, encrypted_key_size, key,
          AES_KEY_SIZE) <= 0) {
    KEY_ERROR (mux, "Failed to encrypt content key\n");
    goto error;
  }

  EVP_PKEY_CTX_free (ctx);
  return TRUE;

error:
  EVP_PKEY_CTX_free (ctx);
error_return:
  return FALSE;
}

void
generate_key (GstMatroskaMux *mux)
{
  guint8 key[AES_KEY_SIZE];
  guint8 *encrypted_key = NULL;
  gsize encrypted_key_size;
  GstBuffer *key_id;
  GstBuffer *prop_key_id = NULL;
  EVP_PKEY *pub_key = NULL;
  int pub_key_size;
  FILE *f;

  if (!RAND_bytes (key, sizeof (key))) {
    KEY_ERROR (mux, "Failed to generate fully random encryption key.\n");
    goto error_return;
  }

  f = fopen (mux->pub_key_prop, "rb");
  if (f == NULL) {
    KEY_ERROR (mux, "Failed to open public key file.\n");
    goto error_return;
  }

  pub_key = PEM_read_PUBKEY (f, &pub_key, NULL, NULL);
  fclose (f);
  if (pub_key == NULL) {
    KEY_ERROR (mux, "Failed to read public key from file.\n");
    goto error_return;
  }

  pub_key_size = EVP_PKEY_size (pub_key);
  if (pub_key_size > MAX_PUBKEY_SIZE) {
    goto error_return;
  }
  encrypted_key_size = (gsize) EVP_PKEY_size (pub_key);
  encrypted_key = g_malloc (encrypted_key_size);
  if (!public_key_encrypt (mux, pub_key, key, encrypted_key,
          &encrypted_key_size)) {
    goto error;
  }
  EVP_PKEY_free (pub_key);
  mux->key = g_bytes_new (key, AES_KEY_SIZE);

  key_id = gst_buffer_new_wrapped (encrypted_key, encrypted_key_size);
  if (mux->key_id_prop != NULL) {
    prop_key_id = prop_to_key_id (&mux->element, mux->key_id_prop);
    if (prop_key_id != NULL) {
      gst_buffer_append (key_id, prop_key_id);
    }
  }
  mux->key_id = key_id;

  set_context (mux);
  return;

error:
  g_free (encrypted_key);
  EVP_PKEY_free (pub_key);
error_return:
  return;
}

void
gst_matroska_mux_set_key (GstMatroskaMux *mux)
{
  GstContext *ctx;

  if (mux->key_prop != NULL && mux->pub_key_prop != NULL) {
    KEY_ERROR (mux,
        "Symmetric key and public key cannot be specified at the same time");
    return;
  }
  if (mux->key_prop == NULL && mux->pub_key_prop == NULL) {
    KEY_ERROR (mux,
        "Attempt to set the encryption key without needed properties");
    return;
  }

  ctx = gst_element_get_context (GST_ELEMENT_PARENT (mux),
      "encryption-context");
  if (ctx != NULL) {
    set_key_from_context (mux, ctx);
  } else if (mux->pub_key_prop != NULL) {
    generate_key (mux);
  } else if (mux->key_prop != NULL) {
    set_key_from_prop (mux);
  }
}

void
gst_matroska_demux_set_keys (GstMatroskaDemux *demux)
{
  if (demux->key_prop != NULL && demux->keys == NULL) {
    gchar *save;
    gboolean ok = TRUE;
    gchar *prop = strtok_r (demux->key_prop, ",", &save);
    demux->keys = g_ptr_array_new_with_free_func (g_free);
    while (prop != NULL && ok) {
      guint8 *key = prop_to_key (&demux->parent, prop);
      if (key != NULL) {
        g_ptr_array_add (demux->keys, key);
        prop = strtok_r (NULL, ",", &save);
      } else {
        ok = FALSE;
      }
    }
    if (!ok) {
      g_ptr_array_free (demux->keys, TRUE);
      demux->keys = NULL;
    }
  }
}
