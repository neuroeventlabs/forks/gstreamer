/* GStreamer EBML I/O
 * (c) 2019 Eckhart Koeppen <eckhart.koppen@neuroeventlabs.com>
 *
 * matroska-keys.h: Get key from key property
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __GST_MATROSKA_KEYS_H__
#define __GST_MATROSKA_KEYS_H__

#include "matroska-mux.h"
#include "matroska-demux.h"

void gst_matroska_mux_set_key (GstMatroskaMux *mux);
void gst_matroska_demux_set_keys (GstMatroskaDemux *demux);

#endif /* __GST_MATROSKA_KEYS_H__ */
